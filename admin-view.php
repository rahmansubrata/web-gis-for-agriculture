<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Map Monitoring</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css"
        integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ=="
   crossorigin=""/> 
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> 
    <style>#map { height: 180px; }</style>
    <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
   integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
   crossorigin=""></script>
</head>
<body>

    <!-- function get data from db -->
    <?php 
    require 'queryFunction.php';
    $getProfileIndustri = ("SELECT * FROM industries_profile");
    $dataIndustri = query($getProfileIndustri);

    $getProfileSensor = ("SELECT c.name_sensor,c.location,c.lat,c.lon,c.pi_index,c.cluster, p1.*
    FROM sensor_profile c
    JOIN data_sensor p1 ON (c.id_sensor = p1.id_sensor)
    LEFT OUTER JOIN data_sensor p2 ON (c.id_sensor = p2.id_sensor AND 
        (p1.time < p2.time OR (p1.time = p2.time AND p1.id < p2.id)))
    WHERE p2.id IS NULL;");
    $dataSensor = query($getProfileSensor);

    $getQuality=("SELECT * FROM calculated_water_quality ORDER BY created_date DESC")
    // var_dump($dataSensor);
    
    ?>
    <!-- add komponen nav-bar -->
    <?php
    include 'pages/nav-bar-admin.php';
    ?>
    <!-- maps peta -->
    <div class="container mt-3 mb-2">
         <div style="height: 800px;" id="map"></div>
    </div>
<script>
    var map = L.map('map').setView([-6.276524,106.851268 ], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'your.mapbox.access.token'
    }).addTo(map);
    var greenIcon = new L.Icon({
        iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-yellow.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
    // var marker = L.marker([-6.276524,106.851268]).addTo(map);
    // looping get data sensor from db
    var markers = [];
    <?php $i = 1; ?>
    <?php 
        foreach ($dataIndustri as $row) :
    ?>
    // [lat,long]
        <?php ?>
        console.log(<?php echo "var{$i}"; ?>)
        var <?php echo "var{$i}"; ?> = L.marker([<?= $row["lat"] ?>,<?= $row["lon"] ?>],{icon: greenIcon}).addTo(map);
        <?php echo "var{$i}"; ?>.bindPopup("<b>Nama industri:<?= $row["name_company"] ?></b><br><b>Jenis Industri :</b><?= $row["industry_type"] ?></b><br><b>Alamat Industri :</b><?= $row["alamat"] ?>").openPopup();
    <?php $i++ ?>
    <?php endforeach; ?>



    // var markerInds = L.marker([-6.2769,106.8518],{icon: greenIcon}).addTo(map);
    // looping get data industri from db
    <?php $i = 1; ?>
    <?php 
        foreach ($dataSensor as $row) :
    ?>
    // [lat,long]
        <?php ?>
        console.log(<?php echo "varSensor{$i}"; ?>)
        var <?php echo "varSensor{$i}"; ?> = L.marker([<?= $row["lat"] ?>,<?= $row["lon"] ?>]).addTo(map);
        <?php echo "varSensor{$i}"; ?>.bindPopup("<b>Waktu Record:<?= $row["time"] ?></b><br><b>Nama Sensor:<?= $row["name_sensor"] ?></b><br><b>id Sensor:<?= $row["id_sensor"] ?></b><br><b>Lokasi :</b><?= $row["location"] ?></b><br><b>Data Monitoring :</b><br>BOD : <?= $row["BOD"] ?><br>COD : <?= $row["COD"] ?> <br>Suhu : <?= $row["suhu"] ?><br>Bakteri Koli : <?= $row["BKOLI"] ?> <br>Fosfat: <?= $row["Fosfat"] ?> <br>TTS : <?= $row["TTS"] ?><br>PI index : <?= $row["pi_index"] ?><br> kelompok  : <?= $row["cluster"] ?>  <br>View Detail data: <a href='/webgis/web-gis-for-agriculture/dataMonitoringAdmin.php?id=<?= $row["id_sensor"] ?>'>link</a> ").openPopup();
    <?php $i++ ?>
    <?php endforeach; ?>


    // var circle = L.circle([-6.276524,106.851268], {
    //                     color: 'red',
    //                     fillColor: '#f03',
    //                     fillOpacity: 0.5,
    //                     radius: 500
    //                 }).addTo(map);

    // var circle2 = L.circle([51.520, -0.11], {
    //                     color: 'blue',
    //                     fillColor: '#f03',
    //                     fillOpacity: 0.5,
    //                     radius: 500
    //                 }).addTo(map);  

    // var polygon = L.polygon([
    //                     [51.509, -0.08],
    //                     [51.503, -0.06],
    //                     [51.51, -0.047]
    //                  ]).addTo(map);
    // marker.bindPopup("<b>Nama Sensor: tets-1</b><br><b>Monitoring air</b><br>BOD:13%.").openPopup();
    // circle.bindPopup("I am a circle.");
    // polygon.bindPopup("I am a polygon.");
    // var popup = L.popup()
    // .setLatLng([51.513, -0.09])
    // .setContent("I am a standalone popup.")
    // .openOn(map);
    var popup = L.popup();

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(map);
    }

    map.on('click', onMapClick);

    // var layer = L.marker(latlng).addTo(map);
    // layer.addTo(map);
    // layer.remove();
</script>

<!-- bootstrap -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>