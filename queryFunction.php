<?php
require 'connectDb.php';

function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

function tambahSensor($data)
{
    global $conn;

    $nama_sensor = $data["nama"];
    $location = $data["lokasi"];
    $query = "INSERT INTO sensor_profile VALUES ('','$nama_sensor','$location','','')";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}
function tambahIndustri($data)
{
    global $conn;

    $name_company = $data["name_company"];
    $industry_type = $data["industry_type"];
    $alamat = $data["alamat"];
    $lon = $data["lon"];
    $lat = $data["lat"];
    $query = "INSERT INTO industries_profile VALUES ('','$name_company','$industry_type','$alamat','$lon','$lat')";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}
