-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2022 at 04:49 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gis_monitoring_water_q`
--

-- --------------------------------------------------------

--
-- Table structure for table `calculated_water_quality`
--

CREATE TABLE `calculated_water_quality` (
  `id` int(11) NOT NULL,
  `id_sensor` int(11) NOT NULL,
  `pi_index` float NOT NULL,
  `cluster` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `calculated_water_quality`
--

INSERT INTO `calculated_water_quality` (`id`, `id_sensor`, `pi_index`, `cluster`, `create_date`) VALUES
(1, 2, 0, 'memenuhi baku mutu (good)', '2022-06-07 14:01:04'),
(2, 2, 0.0519191, 'memenuhi baku mutu (good)', '2022-06-07 14:02:13'),
(3, 2, 0.0519191, 'memenuhi baku mutu (good)', '2022-06-07 14:02:23'),
(4, 2, 0.0519191, 'memenuhi baku mutu (good)', '2022-06-07 14:21:27'),
(5, 2, 0.0519191, 'memenuhi baku mutu (good)', '2022-06-07 14:30:21'),
(7, 2, 0.0519191, 'memenuhi baku mutu (good)', '2022-06-07 14:32:45'),
(8, 2, 0.0519191, 'memenuhi baku mutu (good)', '2022-06-07 14:48:19');

-- --------------------------------------------------------

--
-- Table structure for table `data_sensor`
--

CREATE TABLE `data_sensor` (
  `id` int(11) NOT NULL,
  `id_sensor` int(11) NOT NULL,
  `data` varchar(225) NOT NULL,
  `suhu` float NOT NULL,
  `TDS` float NOT NULL,
  `TTS` float NOT NULL,
  `pH` float NOT NULL,
  `BOD` float NOT NULL,
  `COD` float NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_sensor`
--

INSERT INTO `data_sensor` (`id`, `id_sensor`, `data`, `suhu`, `TDS`, `TTS`, `pH`, `BOD`, `COD`, `time`) VALUES
(7, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:37:01'),
(8, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:38:05'),
(9, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:40:17'),
(10, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:41:50'),
(11, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:42:35'),
(12, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:43:13'),
(14, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:48:52'),
(15, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:49:09'),
(16, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:49:57'),
(17, 1, '12', 12.1, 12.1, 12.1, 12.1, 12.1, 12.1, '2022-05-16 02:50:35'),
(18, 2, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-16 03:00:26'),
(19, 2, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-16 03:00:30'),
(20, 2, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-16 03:00:34'),
(21, 2, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-16 03:17:22'),
(22, 1, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-16 03:17:47'),
(23, 1, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-16 03:29:54'),
(24, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-16 10:49:12'),
(25, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:23:04'),
(26, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:23:49'),
(27, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:24:44'),
(28, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:25:42'),
(29, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:25:48'),
(30, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:25:49'),
(31, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:25:49'),
(32, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:25:50'),
(33, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-05-18 12:25:51'),
(34, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-06-01 06:36:57'),
(35, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-06-01 06:36:59'),
(36, 3, '13', 13.1, 13.1, 13.1, 13.1, 13.1, 13.1, '2022-06-01 06:37:01'),
(37, 3, '13', 13.1, 13, 17, 13.1, 13.1, 13.1, '2022-06-01 07:06:25'),
(38, 3, '13', 13.1, 19, 10, 13.1, 13.1, 13.1, '2022-06-01 07:06:34'),
(39, 3, '13', 13.1, 19, 10, 13.1, 13.1, 13.1, '2022-06-07 13:51:48'),
(40, 3, '13', 13.1, 19, 10, 13.1, 13.1, 13.1, '2022-06-07 13:51:50');

-- --------------------------------------------------------

--
-- Table structure for table `industries_profile`
--

CREATE TABLE `industries_profile` (
  `id` int(11) NOT NULL,
  `name_company` varchar(225) NOT NULL,
  `industry_type` varchar(225) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `lon` float NOT NULL,
  `lat` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `industries_profile`
--

INSERT INTO `industries_profile` (`id`, `name_company`, `industry_type`, `alamat`, `lon`, `lat`) VALUES
(2, 'dwdwd', 'dwd', 'dwdw', 106.852, -6.2769),
(4, 'PT. Suka Bumi Maju', 'tekstil', 'jln. dramaga', 106.859, -6.27999),
(7, 'Pt. sejahterah', 'kimia', 'jln. sumedang', 106.864, -6.28842);

-- --------------------------------------------------------

--
-- Table structure for table `sensor_profile`
--

CREATE TABLE `sensor_profile` (
  `id_sensor` int(11) NOT NULL,
  `name_sensor` varchar(225) NOT NULL,
  `location` varchar(225) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `pi_index` float NOT NULL,
  `cluster` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sensor_profile`
--

INSERT INTO `sensor_profile` (`id_sensor`, `name_sensor`, `location`, `lat`, `lon`, `pi_index`, `cluster`) VALUES
(1, 'test-1234', 'jakarta', -6.276, 106.853, 0.0519191, 'memenuhi baku mutu (good)'),
(2, 'test-2311', 'jakarta', -6.278, 106.833, 0, ''),
(3, 'test-9999', 'jakarta', -6.295, 106.854, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `u_name` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  `user_profile` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calculated_water_quality`
--
ALTER TABLE `calculated_water_quality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_sensor`
--
ALTER TABLE `data_sensor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industries_profile`
--
ALTER TABLE `industries_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensor_profile`
--
ALTER TABLE `sensor_profile`
  ADD PRIMARY KEY (`id_sensor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calculated_water_quality`
--
ALTER TABLE `calculated_water_quality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `data_sensor`
--
ALTER TABLE `data_sensor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `industries_profile`
--
ALTER TABLE `industries_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sensor_profile`
--
ALTER TABLE `sensor_profile`
  MODIFY `id_sensor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
