import math
from xml.etree.ElementTree import PI
import mysql.connector

# Created by @
# Rahman Subrata
# @ Created by

# ini hanya contoh program nya kamu bisa perbaiki dan sesuaikan dengan kebutuhan kamu :)

# membuat koneksi ke database

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="gis_monitoring_water_q"
)
print(mydb)


# ambil rata rata pada selang waktu tertentu ke database

mycursor = mydb.cursor()

# ambil data rata dari tiap parameter dengan id sensor tertentu yang 1 jam kurang dari sekarang
id_sensor="1"
# kamu tinggal tentuin mau pake paramter apa aja ini harus di set ulang
# code ambil data
getTts = "SELECT AVG(TTS) FROM data_sensor WHERE time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) AND id_sensor=3"
getDo = "SELECT AVG(Fosfat) FROM data_sensor WHERE time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) AND id_sensor=3"
getFecal = "SELECT AVG(BKOLI) FROM data_sensor WHERE time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) AND id_sensor=3"
getBod = "SELECT AVG(BOD) FROM data_sensor WHERE time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) AND id_sensor=3"
getSe = "SELECT AVG(COD) FROM data_sensor WHERE time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) AND id_sensor=3"

# executed code ambil data
mycursor.execute(getTts)
TTSresult = mycursor.fetchall()
mycursor.execute(getDo)
DOresult = mycursor.fetchall()
mycursor.execute(getFecal)
Fecalresult = mycursor.fetchall()
mycursor.execute(getBod)
BODresult = mycursor.fetchall()
mycursor.execute(getSe)
Seresult = mycursor.fetchall()

print(TTSresult[0][0])
print(DOresult[0][0])
# nilai hasil pengamatan
TTS_Ci=TTSresult[0][0]
DO_Ci=DOresult[0][0]
Fecal_Ci=Fecalresult[0][0]
BOD_Ci=BODresult[0][0]
Se_Ci=Seresult[0][0]

# nilai kadar mutu yang sudah ditentukan
TTS_Lix=99
DO_Lix=42
pH_Lix=78
Fecal_Lix=123
BOD_Lix=23
Se_Lix=98


# menghitung perbandungan nilai Ci hasil pengamatan dan kadar mutu
Tss_h1=TTS_Ci/TTS_Lix
DO_h1=DO_Ci/DO_Lix
Fecal_h1=Fecal_Ci/Fecal_Lix
BOD_h1=BOD_Ci/BOD_Lix
Se_h1=Se_Ci/Se_Lix


# check Ci/Lxi
if Tss_h1>1:
    # Cij/Lij=1+P Log(Ci/Lij)
    # P: konstata dan nilainya biasanya 5
    Tss_h1=1+5*math.log(TTS_Ci/TTS_Lix)
if DO_h1>1:
    # Cij/Lij=1+P Log(Ci/Lij)
    # P: konstata dan nilainya biasanya 5
    DO_h1=1+5*math.log(DO_Ci/DO_Lix)
if Fecal_h1>1:
    # Cij/Lij=1+P Log(Ci/Lij)
    # P: konstata dan nilainya biasanya 5
    Fecal_h1=1+5*math.log(Fecal_Ci/Fecal_Lix)
if BOD_h1>1:
    # Cij/Lij=1+P Log(Ci/Lij)
    # P: konstata dan nilainya biasanya 5
    BOD_h1=1+5*math.log(BOD_Ci/BOD_Lix)
if Se_h1>1:
    # Cij/Lij=1+P Log(Ci/Lij)
    # P: konstata dan nilainya biasanya 5
    Se_h1=1+5*math.log(Se_Ci/Se_Lix)

# datanya masukin ke array biar bisa di cari rata-ratanya dan nilai maksimalnya
arrData=[Tss_h1,DO_h1,Fecal_h1,BOD_h1,Se_h1]
# print(max(arrData)^2)
# print((sum(arrData)/len(arrData))^2)
# perhitungan Indeks kualitas air
Pi=math.sqrt((max(arrData)**2-(sum(arrData)/len(arrData)))**2/2)
# Pi=7

# hasil akhir kualitas air
if 0<=Pi<=1:
    WaterCuality='memenuhi baku mutu (good)'
if 1<=Pi<=5:
    WaterCuality='tercemar ringan (slightly polluted)'
if 5<Pi<=10:
    WaterCuality='tercemar sedang (fairly polluted)'
if Pi>=10:
    WaterCuality='tercemar berat (Highest polluted'
print(WaterCuality)
# simpan data ke Db datanya ( nanti aja pas udah fix aku kerjain biar sekalian)
sql = "INSERT INTO calculated_water_quality (id_sensor, pi_index,cluster) VALUES (%s, %s,%s)"

val = (2, Pi,WaterCuality)

mycursor.execute(sql, val)


mydb.commit()


print(mycursor.rowcount, "record inserted.")

# sql2 = "INSERT INTO sensor_profile (pi_index,cluster) VALUES (%s, %s) WHERE id_sensor=1"
sql = "UPDATE sensor_profile SET pi_index =%s,cluster=%s WHERE id_sensor=3"
val = ( Pi,WaterCuality)
mycursor.execute(sql,val)

mydb.commit()

print(mycursor.rowcount, "record(s) affected")