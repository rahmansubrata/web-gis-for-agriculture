<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    include_once 'Database.php';
    include_once 'dataSensorMonitoring.php';
    $database = new Database();
    $db = $database->getConnection();
    $item = new DataMonitoring($db);
    $dataJson = json_decode(file_get_contents("php://input"));
    $item->id_sensor = $dataJson->id_sensor;
    $item->data = $dataJson->data;
    $item->suhu = $dataJson->suhu;
    $item->Fosfat = $dataJson->Fosfat;
    $item->TTS = $dataJson->TTS;
    $item->BKOLI = $dataJson->BKOLI;
    $item->BOD = $dataJson->BOD;
    $item->COD = $dataJson->COD;
    $item->lat = $dataJson->lat;
    $item->lon = $dataJson->lon;
    
    if($item->createData()){
        echo 'Data was created successfully.';
    } else{
        echo 'Data could not be created.';
    }
?>