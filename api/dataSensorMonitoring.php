<?php
    class DataMonitoring{
        // Connection
        private $conn;
        // Table
        private $db_table = "data_sensor";
        // Columns
        public $id;
        public $id_sensor;
        public $data;
        public $suhu;
        public $Fosfat;
        public $TTS;
        public $Bkoli;
        public $BOD;
        public $COD;

        private $db_tabel_sensor="sensor_profile";
        // columns
        public $lon;
        public $lat;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }
   
        // CREATE
        public function createData(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        id_sensor = :id_sensor, 
                        data = :data,
                        suhu= :suhu,
                        Fosfat= :Fosfat,
                        TTS= :TTS,
                        BKOLI= :BKOLI,
                        BOD= :BOD,
                        COD= :COD";

            $sqlQuery2="UPDATE ".$this->db_tabel_sensor." SET 
                        lat=:lat,
                        lon=:lon
                        WHERE
                        id_sensor=:id_sensor
                        ";
        
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt2 = $this->conn->prepare($sqlQuery2);
            // sanitize
            $this->id_sensor=htmlspecialchars(strip_tags($this->id_sensor));
            $this->data=htmlspecialchars(strip_tags($this->data));
            $this->suhu=htmlspecialchars(strip_tags($this->suhu));
            $this->Fosfat=htmlspecialchars(strip_tags($this->Fosfat));
            $this->TTS=htmlspecialchars(strip_tags($this->TTS));
            $this->BKOLI=htmlspecialchars(strip_tags($this->BKOLI));
            $this->BOD=htmlspecialchars(strip_tags($this->BOD));
            $this->COD=htmlspecialchars(strip_tags($this->COD));
            $this->lon=htmlspecialchars(strip_tags($this->lon));
            $this->lat=htmlspecialchars(strip_tags($this->lat));
        
            // bind data
            $stmt->bindParam(":id_sensor", $this->id_sensor);
            $stmt->bindParam(":data", $this->data);
            $stmt->bindParam(":suhu", $this->suhu);
            $stmt->bindParam(":Fosfat", $this->Fosfat);
            $stmt->bindParam(":TTS", $this->TTS);
            $stmt->bindParam(":BKOLI", $this->BKOLI);
            $stmt->bindParam(":BOD", $this->BOD);
            $stmt->bindParam(":COD", $this->COD);
            $stmt2->bindParam(":lat", $this->lat);
            $stmt2->bindParam(":lon", $this->lon);
            $stmt2->bindParam(":id_sensor", $this->id_sensor);
            if($stmt->execute()   ){
                $stmt2->execute();
                return true;
            }
            return false;
        }
        // GET ALL NOT USE
        public function getEmployees(){
            $sqlQuery = "SELECT id, name, email, age, designation, created FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }
        // READ single NOT USE
        public function getSingleEmployee(){
            $sqlQuery = "SELECT
                        id, 
                        name, 
                        email, 
                        age, 
                        designation, 
                        created
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->name = $dataRow['name'];
            $this->email = $dataRow['email'];
            $this->age = $dataRow['age'];
            $this->designation = $dataRow['designation'];
            $this->created = $dataRow['created'];
        }        
        // UPDATE NOT USE
        public function updateEmployee(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        email = :email, 
                        age = :age, 
                        designation = :designation, 
                        created = :created
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->age=htmlspecialchars(strip_tags($this->age));
            $this->designation=htmlspecialchars(strip_tags($this->designation));
            $this->created=htmlspecialchars(strip_tags($this->created));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":age", $this->age);
            $stmt->bindParam(":designation", $this->designation);
            $stmt->bindParam(":created", $this->created);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // DELETE NOT USE
        function deleteEmployee(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
?>