<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Tambah Sensor Monitoring</title>

    <!-- data tabel asset -->
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  </head>
  <body>
  <?php
  require 'queryFunction.php';
  $id_sensor= $_GET["id"];

  $getDataSensor = ("SELECT * FROM data_sensor WHERE id_sensor=$id_sensor");
  $data = query($getDataSensor);
  $getProfil=("SELECT * FROM sensor_profile WHERE id_sensor=$id_sensor");
  $dataSensor=query($getProfil);
  
  ?>   
  <!-- add komponan navbar -->
    <?php
    include 'pages/nav-bar-admin.php';
    ?>
    <div class="container mt-5">
        <h3 class="mt-3 text-center">Detail Data Monitoring</h3>
        <p>Nama sensor : <?= $dataSensor[0]["name_sensor"] ?> </p>
        <p>lokasi : <?= $dataSensor[0]["location"] ?> </p>
        <p>latitude : <?= $dataSensor[0]["lat"] ?> </p>
        <p class="mb-5">langitude : <?= $dataSensor[0]["lon"] ?> </p>
        <table  id="tabel-data"class="table table-hover mt-5">
            <thead>
                <tr>
                    <th scope="col">no</th>
                    <th scope="col">Waktu Pengambilan</th>
                    <th scope="col">BOD</th>
                    <th scope="col">COD</th>
                    <th scope="col">Suhu</th>
                    <th scope="col">Bakteri Koli</th>
                    <th scope="col">Fosfat</th>
                    <th scope="col">TTS</th>
                </tr>     
            </thead>
            <tbody>
            <?php $i = 1; ?>
                <?php foreach ($data as $row) : ?>
                    <tr>
                        <th scope="row"><?= $i ?></th>
                        <td><?= $row["time"] ?></td>
                        <td><?= $row["BOD"] ?></td>
                        <td><?= $row["COD"] ?></td>
                        <td><?= $row["suhu"] ?></td>
                        <td><?= $row["BKOLI"] ?></td>
                        <td><?= $row["Fosfat"] ?></td>
                        <td><?= $row["TTS"] ?></td>
                    </tr>
                    <?php $i++ ?>
            <?php endforeach; ?>
            </tbody>
            <script>
                $(document).ready(function(){
                    $('#tabel-data').DataTable();
                });
            </script>
        </table>
        

    </div>
    <!-- footer -->
    <?php include 'pages/footer.php'?>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>