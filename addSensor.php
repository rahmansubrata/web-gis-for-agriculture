<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Tambah Sensor Monitoring</title>
    <!-- data tabel asset -->
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  </head>
  <body>
  <?php
  require 'queryFunction.php';
  $getProfileSensor = ("SELECT * FROM sensor_profile");
  $data = query($getProfileSensor);

  if (isset($_POST["submit"])) {
        if (tambahSensor($_POST) > 0) {
            echo "<script>
        alert('Data Berhasil Ditambahkan');
        document.location.href = 'addSensor.php';
      </script>
    ";
        } else {
            echo "<script>
                  alert('Data Gagal Ditambahkan');
                  document.location.href = 'addSensor.php';
                </script>
            ";
        }
    }
  ?>   
  <!-- add komponan navbar -->
    <?php
    include 'pages/nav-bar-admin.php';
    ?>
    <div class="container mt-5">
        <h3 class="mt-3 text-center">Daftar Sensor</h3>
        <table  id="tabel-data" class="table table-hover">
        <thead>
          <tr>
            <th scope="col">no</th>
            <th scope="col">Nama Seri Sensor</th>
            <th scope="col">Lokasi</th>
            <th scope="col">latitude</th>
            <th scope="col">longitude</th>
          </tr>
        </thead>
        <tbody>
        <?php $i = 1; ?>
            <?php foreach ($data as $row) : ?>
                <tr>
                    <th scope="row"><?= $i ?></th>
                    <td><?= $row["name_sensor"] ?></td>
                    <td><?= $row["location"] ?></td>
                    <td><?= $row["lat"] ?></td>
                    <td><?= $row["lon"] ?></td>
                </tr>
                <?php $i++ ?>
        <?php endforeach; ?>
        </tbody>
        <script>
                $(document).ready(function(){
                    $('#tabel-data').DataTable();
                });
            </script>
        </table>
        <h3 class="mt-5">Tambah Titik Monitoring</h3>
        <form action="" method="post">
          <div class="mb-3 mt-2">
              <label for="nama" class="form-label">Nama Sensor</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="no seri of sensor">
          </div>
          <div class="mb-3">
              <label for="lokasi" class="form-label">Location Sensor</label>
              <input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="location sensor">
          </div>
          <div class="d-grid gap-2">
              <button class="btn btn-primary" type="submit" name="submit">Tambah</button>
          </div>
        </form>


        

    </div>
    <!-- footer -->
    <?php include 'pages/footer.php'?>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>